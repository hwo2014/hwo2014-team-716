# TODO

## Ideas

### Throttle control

Some PID that reads the position angle, and corrects the throttle

### Lane control (Track optimizer)

Find the best path along the track. Also figure out how to pass other cars.

### Game model reverse engineering

Figure out the rules of the game world

 * Can the tires slide?
 * Does drifting (nonzero angle) affect the speed?
 * Is the game deterministic?
 * Is there mass/momentum of the cars?
 * What is the model of the physics?
