import json
import logging


class BaseBot(object):
    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

        logging.getLogger().setLevel(logging.INFO)

        self.msg_map = {
            'join': self.on_join,
            'yourCar': self.on_car,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'turboAvailable': self.on_turbo_available,
            'lapFinished': self.on_lap_finished,
            'finish': self.on_finish,
            'gameEnd': self.on_game_end,
            'tournamentEnd': self.on_tournament_end,
            'crash': self.on_crash,
            'spawn': self.on_spawn,
            'error': self.on_error,
        }

        self.currentTick = 0
        self.currentPositions = None
        self.previousPositions = None

    # Game state
    def get_speed(self):
        if not (self.previousPositions and self.currentPositions):
            return None

    def get_own_position(self, positions):
        for position in positions:
            if position['id']['name'] == self.name:
                return position

    # Messaging
    def msg(self, msg_type, data):
        self.send(json.dumps({'msgType': msg_type, 'data': data}))

    def send(self, msg):
        self.socket.send(msg + '\n')

    def join(self):
        return self.msg('join', {'name': self.name,
                                 'key': self.key})

    def throttle(self, throttle):
        self.msg('throttle', throttle)

    def switch_lane(self, direction):
        self.msg('switchLane', direction)

    def ping(self):
        self.msg('ping', {})

    # Run loop
    def run(self):
        self.join()
        self.msg_loop()

    def msg_loop(self):
        socket_file = self.socket.makefile()
        for line in socket_file:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            logging.debug('%s: %r', msg_type, data)
            msg_handler = self.msg_map.get(msg_type)
            if msg_handler:
                msg_handler(data)
            else:
                logging.warn("Unkown message received: %s", msg_type)
                self.ping()

    # Events
    def on_join(self, data):
        logging.info('Joined game with name: %s', data['name'])
        self.ping()

    def on_car(self, data):
        logging.info('Got car color: %s', data['color'])
        self.ping()

    def on_game_init(self, data):
        logging.info('Game initialized on track %s.', data['race']['track']['name'])
        #print json.dumps(data['race'], indent=4, separators=(',', ': '))
        self.ping()

    def on_game_start(self, data):
        logging.info('Game started.')
        self.ping()

    def on_car_positions(self, data):
        logging.debug('Position updated: %s', data)
        self.throttle(0.5)

    def on_turbo_available(self, data):
        logging.info('Turbo available.')
        self.ping()

    def on_lap_finished(self, data):
        logging.info('Lap finished in %d ms.', data['lapTime']['millis'])
        self.ping()

    def on_finish(self, data):
        logging.info('Race finished.')
        self.ping()

    def on_game_end(self, data):
        logging.info('Game ended.')
        for result in data['results']:
            logging.info('%s: %d ms', result['car']['name'], result['result'].get('millis'))
        self.ping()

    def on_tournament_end(self, data):
        logging.info('Tournament ended.')
        self.ping()

    def on_crash(self, data):
        logging.info('Crashed: %s', data)
        self.ping()

    def on_spawn(self, data):
        logging.info('Spawned: %s', data)
        self.ping()

    def on_error(self, data):
        logging.warn('Error: %s', data)
        self.ping()
