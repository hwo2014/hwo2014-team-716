# -*- coding: utf-8 -*-
import base
import logging

from routers.shortest_path import ShortestPathRouter


class RouterBot(base.BaseBot):
    def __init__(self, socket, name, key):
        super(RouterBot, self).__init__(socket, name, key)
        self.router = ShortestPathRouter()

    def on_game_init(self, data):
        super(RouterBot, self).on_game_init(data)
        self.router.set_track(data['race']['track'])

    def on_car_positions(self, data):
        own_pos = self.get_own_position(data)['piecePosition']

        switch = self.router.get_switch(own_pos)
        if switch is None:
            self.throttle(0.6)
        else:
            self.switch_lane(switch)
