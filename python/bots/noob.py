# -*- coding: utf-8 -*-
import base
import logging


class NoobBot(base.BaseBot):
    def on_car_positions(self, data):
        own_data = data[0]
        own_pos = own_data['piecePosition']
        logging.info('Position updated: %d-%f %f˚', own_pos['pieceIndex'], own_pos['inPieceDistance'], own_data['angle'])
        self.throttle(0.8)
