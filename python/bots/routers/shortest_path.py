import math


class ShortestPathRouter(object):

    def __init__(self):
        self.track = None

    def _create_segment(self):
        return {
            "pieces": [],
            "lane_lengths": [0]*len(self.track['lanes'])
        }


    def set_track(self, track):
        self.track = track
        lanes = track['lanes']

        segments = []
        current_segment = self._create_segment()
        segments.append(current_segment)

        # Calculate lane lengths
        for piece_id, piece in enumerate(track['pieces']):
            if piece.get('switch', False):
                current_segment = self._create_segment()
                segments.append(current_segment)

            current_segment['pieces'].append(piece_id)

            for lane_id, lane in enumerate(lanes):
                angle = piece.get('angle')
                if angle is None:
                    lane_length = piece['length']
                else:
                    lane_radius = piece['radius'] - lane['distanceFromCenter']
                    lane_length = abs(math.pi * angle * lane_radius) / 180.0
                current_segment['lane_lengths'][lane_id] += lane_length

        # Join first and last
        if len(segments) > 1:
            last_segment = segments.pop()
            first_segment = segments[0]
            first_segment['pieces'] = first_segment['pieces'] + last_segment['pieces']
            for lane_id, lane_length in enumerate(last_segment['lane_lengths']):
                first_segment['lane_lengths'][lane_id] += lane_length

        # Get shortest lanes
        for segment in segments:
            lane_lengths = segment['lane_lengths']
            shortest_lane = lane_lengths.index(min(lane_lengths))

            for piece_id in segment['pieces']:
                self.track['pieces'][piece_id]['preferred_lane'] = shortest_lane

    def get_switch(self, position):
        """
            Given the current car position, return 'left' if the car should switch left, 'right' if the car should
            switch lanes to the right, and None if the car shouldn't switch lanes.
        """
        current_piece_index = position['pieceIndex']
        current_piece = self.track['pieces'][current_piece_index]
        if not current_piece.get('switch', False):
            return None

        car_lane = position['lane']['startLaneIndex']
        preferred_lane = current_piece['preferred_lane']
        if car_lane == preferred_lane:
            return None
        elif car_lane < preferred_lane:
            return 'Right'
        elif car_lane > preferred_lane:
            return 'Left'
