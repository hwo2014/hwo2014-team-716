import unittest
import json
import os

from bots.routers.shortest_path import ShortestPathRouter

class ShortestPathTest(unittest.TestCase):
    def setUp(self):
        self.router = ShortestPathRouter()

    def test_keimola(self):
        with open(os.path.dirname(os.path.abspath(__file__)) + '/data/keimola.json', 'r') as track_file:
            track = json.load(track_file)
        self.router.set_track(track)
