import socket
import sys
import argparse

from bots import noob
from bots import router


def main():
    config = get_config()
    print("Connecting with parameters:")
    print("host={host}, port={port}, bot name={bot_name}, key={key}".format(**vars(config)))
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((config.host, int(config.port)))
    # bot = noob.NoobBot(s, config.bot_name, config.key)
    bot = router.RouterBot(s, config.bot_name, config.key)
    bot.run()


def get_config():
    parser = argparse.ArgumentParser(description='KSDL game client.')
    parser.add_argument('host', help='game server host name')
    parser.add_argument('port', help='game server port')
    parser.add_argument('bot_name', help='name of the bot')
    parser.add_argument('key', help='client key')

    args = parser.parse_args()
    return args


if __name__ == "__main__":
    main()
